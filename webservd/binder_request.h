// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WEBSERVER_BINDER_REQUEST_H_
#define WEBSERVER_BINDER_REQUEST_H_

#include <map>
#include <string>
#include <vector>

#include <android-base/unique_fd.h>

#include "android/webservd/BnHttpRequest.h"

#include "webservd/request.h"

namespace android {
namespace webservd {

// TODO(sadmac): File support
class HttpRequest final : public android::webservd::BnHttpRequest {
 public:
  explicit HttpRequest(::webservd::Request* request) : request_(request) {}

  android::binder::Status GetBody(android::base::unique_fd* body) override;
  android::binder::Status GetUrl(std::string* url) override;
  android::binder::Status GetMethod(std::string* method) override;
  android::binder::Status GetQueryParams(std::vector<std::string>* params)
      override;
  android::binder::Status GetPostParams(std::vector<std::string>* params)
      override;
  android::binder::Status GetHeaders(std::vector<std::string>* headers)
      override;
  android::binder::Status Respond(
      int32_t status_code,
      const std::vector<std::string>& response_headers,
      int32_t data_size,
      android::base::unique_fd* response_stream) override;

 private:
  ::webservd::Request* request_;
  android::base::unique_fd body_;

  DISALLOW_COPY_AND_ASSIGN(HttpRequest);
};

}  // namespace webservd
}  // namespace android

#endif  // WEBSERVER_BINDER_REQUEST_H_
