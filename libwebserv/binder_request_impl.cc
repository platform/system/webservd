// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "libwebserv/binder_request_impl.h"

using std::string;
using std::vector;

namespace libwebserv {

BinderRequestImpl::BinderRequestImpl(const string& url,
                                     const string& method,
                                     const vector<string>& headers,
                                     const vector<string>& query_params,
                                     const vector<string>& post_params,
                                     android::base::unique_fd&& body)
    : Request(url, method),
      body_(std::move(body)) {
  CHECK(!(headers.size() % 2));
  CHECK(!(query_params.size() % 2));
  CHECK(!(post_params.size() % 2));

  for (auto it = headers.cbegin(); it != headers.cend(); it += 2) {
    headers_.emplace(*it, *(it + 1));
  }

  for (auto it = query_params.cbegin(); it != query_params.cend(); it += 2) {
    get_data_.emplace(*it, *(it + 1));
  }

  for (auto it = post_params.cbegin(); it != post_params.cend(); it += 2) {
    post_data_.emplace(*it, *(it + 1));
  }
}

brillo::StreamPtr BinderRequestImpl::GetDataStream() {
  brillo::ErrorPtr error;
  brillo::StreamPtr body_stream =
      brillo::FileStream::FromFileDescriptor(body_.get(), false, &error);

  if (error) {
    body_stream.reset();
  }

  return body_stream;
}

}  // namespace libwebserv
